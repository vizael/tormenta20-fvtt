---
title: "Exemplos de Efeitos"
weight: 4
---

Demonstrar o processo de configuração de alguns efeitos.

## Magias

#### Armadura Arcana

Armadura Arcana possui 4 efeitos, sendo um temporário e três aprimorramentos.

O efeito temporário Armadura Arcana servirá de base para gerar efeito final, com os possíveis aprimoramenteos. Assim:
- **Efeito Interrompido** deve permanceer desmarcado; Para que ele apareça mesmo que nenhum aprimoramento seja aplicado;
- **Tranferir para Ator** deve estar desmarcado; Pois ele pode possuir Aprimoramentos que mudem seus valores;
- **Duração Especial** cena deve estar marcado, para que o efeito seja encerrado quando uma cena for encerrada;
- **Efeitos**
    - *data.defesa.armadura.value — Atualizar — 4*: Irá substituir o bônus de armadura para 4, se for maior que o bônus atual de armadura;

Efeitos de Uso / Aprimoramentos
1. muda para reação
    - **Aplicável em** marcado como próprio item; Pois é usável somente quando conjurando Armadura Arcana;
    - **Efeitos**
        - *execucao — Substituir — reação*              : substitui Padrão por Reação;
        - *duracao — Substituir — 1 turno*              : substitui Cena por 1 turno;
2. aumenta o bônus em +1.
    - **Permite múltiplas aplicações** marcado pois é um aprimoramento do tipo aumenta.
    - **Efeitos**
        - *data.defesa.armadura.value — Adicionar — 1*  : aumenta em 1 o bônus da Armadura Arcana;
3. aumenta o bônus em +1.
    - **Efeitos**
        - *duracao — Substituir — 1 dia*                : substitui cena por 1 dia;

#### Área Escorregadia

1. aumenta a área
    - **Efeitos**
        - *area — Adicionar — 1,5m*       : aumenta o tamanho do quadrado em 1,5m;

#### Área Escorregadia

3. duração 1 dia...
    - **Efeitos**
        - *condicao — Customizar — Paralisado*       : Inclui a condição no card somente quando este aprimoramento é aplicado;

#### Curar Ferimentos

1. Truque estabilizar;
    - **Custo** em branco torna este aprimoramento um Truque;
    - **Efeitos**
        - *roll — Substituir — *       : Não rolará dados;
2. Truque dano em mortos vivos;
    - **Efeitos**
        - *roll — Customizar — 1d8*       : Rolará apenas 1d8 ao invéz de 2d8+2;

3. Truque dano em mortos vivos;
    - **Permite múltiplas aplicações** marcado pois é um aprimoramento do tipo aumenta.
    - **Efeitos**
        - *roll — Customizar — 1d8+1*       : Adicionará mais 1d8+1 pra cada aplicação deste aprimoramento;

>  Caso fosse usado o modo Adicionar a rolagem final seria 2d8+2 + 1d8+1 e não seria afetada por multiplas aplicações

## Habilidades e Poderes

#### Ataque Furtivo
1. Ataque Furtivo
    - **Aplicável em** será aplicado somente nos ataques;
    - **Efeitos**
        - *dano — Adicionar — roll*       : Adicionará mais a Rolagem do Poder à jogada de dano;

#### Golpe Divino

1. Golpe Divino
    - **Aplicável em** será aplicado somente nos ataques;
    - **Efeitos**
        - *ataque — Adicionar — @car*       : Adicionará mais o Modificador de carisma jogada de ataque;
        - *dano — Adicionar — 1d8*       : Adicionará 1d8 a jogada de dano;
2. aumenta o dano
    - **Aplicável em** será aplicado nos ataques e no próprio Poder, caso prefira joga-los separadamente;
    - **Efeitos**
        - *dano — Customizar — 1d8*       : Adicionará 1d8 ao dano do Golpe Divino;
        - *roll — Customizar — 1d8*       : Adicionará 1d8 ao dano do Golpe Divino, quando rolando pelo poder;

####  Arma Sagrada

1. Arma Sagrada
    - **Efeitos**
        - *@Golpe Divino#dano — Customizar — d12*       : Mudará o dado do poder Golpe Divino para d12;
