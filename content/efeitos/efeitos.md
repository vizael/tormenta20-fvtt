---
title: "Introdução"
weight: 1
---

Efeitos são condições que, quando ativas, alteram características de um personagem, as condições são efeitos, buffs provenientes de habilidades são efeitos.


> EFEITOS DE ITENS SÓ PODEM SER EDITADOS SE O ITEM NÃO ESTIVER EM UMA FICHA.
>
> SENDO NECESSÁRIO ARRASTAR O ITEM PARA O DIRETÓRIO DE ITENS E EDITÁ-LO,
> E ARRASTA-LO DE VOLTA PARA A FICHA;
> 
> EFEITOS SÓ FUNCIONAM EM TOKENS LINKADOS A FICHAS (PERSONAGENS).

[![GUIA EM VIDEO DO MESTRE DIGITAL](https://img.youtube.com/vi/iG2yent-GNY/0.jpg)](https://www.youtube.com/watch?v=iG2yent-GNY)

Possuem quatro categorias:

{{< figure src="../../images/efeitos.jpg" width="512px" class="float-right" >}}

1. **Efeitos de Uso:** criados para o Tormenta20, são efeitos aplicados no uso de uma habilidade, como os aprimoramentos de magia ou poderes como Golpe Divino;

2. **Efeitos Temporários:** São efeitos com duração pré-determinada, sua duração é reduzida nos avanços de turnos e rodadas de combate. São exibidos nos Tokens;
    - Tormenta20 tem efeitos que nem sempre são iguais, nesses casos o efeito não deve ser passado para o Personagem, ele servirá de base para criar o efeito final com seus possíveis aprimoramentos.

3. **Efeitos Passívos:** são efeitos sem duração;

4. **Efeitos Inativos:** enquanto inativos, não alteram características


Quando habilidades possuem efeitos aplicáveis sua mensagem do chat exibirá botões para aplicá-los. Selecione os tokens e clique no botão.

{{< figure src="../../images/chat-efeitos.jpg" width="512px" >}}


<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>