---
title: "Configurando Efeitos"
weight: 2
---

Um efeito temporário ou passivo.

## Detalhes

{{< figure src="../../images/efeitos-detalhe.jpg" width="512px" >}}
1. Nome do Efeito e Icone do Efeito
2. **Efeito Interrompido:** Se o efeito está ativo ou inativo.
3. **Transferir para o Ator:** Se o efeito vai transferir suas alterações para o Personagem.
4. **Origem:** O Código do Item ou Personagem que está gerando aquele efeito.

## Duração

{{< figure src="../../images/efeitos-duration.jpg" width="512px" >}}
- **Cena:** caso marcado, o efeito será terminado quando clicar no botão encerrar cena a aba de monitor de combate do menu lateral.

## Efeitos

{{< figure src="../../images/efeitos-changes.jpg" width="512px" class="float-right" >}}
Aqui são configuradas as alterações que aquele efeito causa no personagem;

- **Atributo-chave:** a característica que aquele efeito vai alterar (Veja os campos existentes abaixo)
- **Mudar Modo:** o tipo de alteração
    - *Customizar:* Situações especiais.
    - *Multiplicar:*  Multiplica o valor existente pelo Valor do Efeito.
    - *Adicionar:* Soma com o valor existente.
    - *Retroceder:* Substitui se o Valor do Efeito for menor que o original.
    - *Atualizar:* Substitui se o Valor do Efeito for maior que o original.
    - *Substituir:* O valor existente é substituido pelo Valor do Efeito.
- **Valor do Efeito:** o valor a ser aplicado de acordo com o modo escolhido.

## Atributos de personagens

| Atributo-chave                       | Cáracterística alterada                           |
| ------------------------------------ | ------------------------------------------------- |
| data.attributes.movement.???         | Deslocamento (? = walk, swim, fly, burrow, climb) |
| data.rd.???                          | Redução de dano (value, base, temp)               |
| data.defesa.???                      | Defesa (final, outro, bonus)                      |
| data.defesa.bonus                    | Para dados ou características ex. 1d6, @car       |
| data.atributos.???.value             | Atributo - Valor Base (for,des,con,int,sab,car)   |
| data.atributos.???.temp              | Atributo - Valor Temporário                       |
| data.pericias.???.outros             | Pericias - Outros Bônus                           |
| data.pericias.???.treino             | Pericias - Bônus de Treinamento                   |
| data.pericias.???.temp               | Pericias - Bônus Temporário ex. @car              |
| data.pericias.ofi.mais.###.value     | Para pericias de oficio (0, 1, 2, 3)              |
| data.periciasCustom.###.value        | Para pericias customizadas (0, 1, 2, 3)           |
| data.tamanho                         | Tamanho ("grande","medio", "minusculo", ...)      |

## Atributos de Modificadores

Esses campos não alteram caracteríscias dos personagens, mas são consultados quando uma rolagem é feita e seu valor adicionado como um Bonus/Penalidade.


| Atributo-chave                            | Usado em Testes de Atritubo                       |
| ----------------------------------------- | ------------------------------------------------- |
| data.modificadores.atributos.geral        | Afeta todos os atributos                          |
| data.modificadores.atributos.fisicos      | Afeta atributos físicos (for,des,con)             |
| data.modificadores.atributos.mentais      | Afeta atributos mentais (int,sab,car)             |

| Atributo-chave                            | Usado em Testes de Perícias                       |
| ----------------------------------------- | ------------------------------------------------- |
| data.modificadores.pericias.geral         | Afeta todas as perícias                           |
| data.modificadores.pericias.semataque     | Afeta todas as perícias exceto Luta e Pontaria    |
| data.modificadores.pericias.ataque        | Afeta somente as perícias usadas ao atacar        |
| data.modificadores.pericias.resistencia   | Afeta somente Fortitude, Reflexos e Vontade       |
| data.modificadores.pericias.atr.???       | Afeta somente pericias daquele atributo (for,des) |

| Atributo-chave                            | Usado em Rolagens de Dano                         |
| ----------------------------------------- | ------------------------------------------------- |
| data.modificadores.dano.geral             | Afeta todas as Rolagens de Dano                   |
| data.modificadores.dano.cac               | Afeta Rolagens de Dano de ataques com Luta        |
| data.modificadores.dano.ad                | Afeta Rolagens de Dano de ataques com Pontaria    |
| data.modificadores.dano.mag               | Afeta Rolagens de Dano de Magias                  |

<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>