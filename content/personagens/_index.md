---
title: "Personagens"
weight: 2
---

Personagens no FoundryVTT são usualmente chamados de Atores (Actors).

Em Tormenta 20 existem dois tipos de Atores: Personagens e NPCs.