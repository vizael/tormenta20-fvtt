---
title: "Ficha Personagem"
weight: 1
---

Personagens no FoundryVTT são usualmente chamados de Atores (Actors).

Em Tormenta 20 existem dois tipos de Atores: Personagens e NPCs.

Personagens possuem duas fichas, uma padrão e uma com abas.

[![GUIA EM VIDEO DO MESTRE DIGITAL](https://img.youtube.com/vi/pPFfXaQ_zAU/0.jpg)](https://www.youtube.com/watch?v=pPFfXaQ_zAU)

## Ficha
{{< figure src="../../images/ficha-actor.jpg" width="512px" class="float-right" >}}

Ao lado um exemplo da ficha padrão.

1. Abas
    - Ficha: contém as caracteristicas de um personagem;
    - Diário: encontram-se uma série de campos para anotações e descrições;
    - Efeitos: uma lista de habilidades, condições e buffs;
    - Engrenagens: abre um menu de configuração;
2. Cabeçalho da ficha
    - Nível: você pode configurar modificadores de PV/PM por nível;
3. Atributos e Pontos de vida
4. Listas de itens
    - Clicar no nome exibira sua descrição;
    - Clicar com o botão direito do mouse no nome abrirá para edição;
    - Clicar no dado usará o item (segurando shift irá a abrir a janela de uso onde pode aplicar aprimoramentos, poderes, etc);
    - Itens (qualquer material do compendium, exceto classes, podem ser arrastados para a barra de atalhos);
    - 4.1. Lista de Armas
    - 4.2. Lista de Equipáveis
        - Clicar no escudo alterna entre equipado/não equipado;
    - 4.3. Lista de Poderes
    - 4.4. Lista de Magias
        - CD Base sobrescreve o valor base da CD;
        - Atributo de conjuração (novas magias virão configuradas com este atributo)
    - 4.5. Lista de Inventário
5. Defesa
6. Características
    - Deslocamento: possui um menu pra cada tipo de deslocamento.
    - Classes: são incluidas arrastando do compêndio, ou do Diretório de Itens. Clicar nas classes permite configurá-la;
7. Pericias
    - Também podem ser arrastadas para a barra de atalhos.
    - Clicar com o botão direito do mouse abrirá o diário com os usos da perícias;
8. Moedas
9. Carga

## Configurações

### Configurações de Personagem
Clicando na engrenagem;
{{< figure src="../../images/config-actor.jpg" width="512px" >}}

1. Exibe os campos opcionais de bônus temporário;
2. Trava a edição das perícias;
3. Habilita a marcação de magias preparadas;

### Configurações de Nível
Clicando no nível do personagem;
{{< figure src="../../images/config-nivel.jpg" width="512px" >}}

1. Mostra os pontos fornecidos pelas classes;
2. Incluí o mod da perícia marcada no total de PV/PM
3. Incluí o valor passado no total de PV/PM
4. Incluí o valor pelo nível; (ganhos por nível par podem ser somados como 0.5)


### Configurações de Deslocamento e Proficiências
Clicando no nome das Características (deslocamento, armas, armaduras);
{{< figure src="../../images/config-traits.jpg" width="512px" >}}


<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>