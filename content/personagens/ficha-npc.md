---
title: "Ficha NPC"
weight: 3
---

A ficha de NPC seguem um layout minimizado;

## Ficha


1. Abas
    - Editar: alterna o modo edição, exibindo os campos da ficha;


{{< figure src="../../images/ficha-npc.jpg" width="512px" class="float-right" >}}
{{< figure src="../../images/ficha-npc-edit.jpg" width="512px" class="float-right" >}}


<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>