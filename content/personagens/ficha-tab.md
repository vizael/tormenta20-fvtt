---
title: "Ficha com Abas"
weight: 2
---

A ficha de abas é uma Configuração do Sistema para cada usuário.

{{< figure src="../../images/config-ficha.jpg" width="900px" >}}

## Ficha Abas

{{< figure src="../../images/ficha-tab.jpg" width="512px" class="float-right" >}}

10. Favoritos
    - Items favoritados (a estrela ao lado direito) aparecerão aqui;

As funcionalidades são as mesmas da ficha padrão, mudando apenas sua disposição;

<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>