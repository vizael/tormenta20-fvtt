---
title: "Home"
---

# Home

### Bem vindo a documentação do Tormenta20 no Foundry VTT!

Esta documentação serve como um repositório de informações reunidas pela comunidade para o uso do sistema **Tormenta20** no **Foundry Virtual Tabletop** e é gerenciado e mantido por membros da comunidade. Não representamos a Jambo Editora ou Foundry Gaming, LLC.
