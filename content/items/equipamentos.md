---
title: "Equipamentos"
weight: 3
---

## Detalhes
{{< figure src="../../images/ficha-equipamentos.jpg" width="512px" class="float-right" >}}

Equipamentos possuem sete categorias

- Armadura Leve/Pesada e Escudos só podem haver uma equipados, equipar um novo irá remover o bônus do anterior.
- Armauduras Pesadas não somam o modificador de Destreza;




<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>