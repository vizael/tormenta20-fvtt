---
title: "Habilidades"
weight: 4
---

# Magias

## Detalhes
{{< figure src="../../images/ficha-magia.jpg" width="512px" class="float-right" >}}

1. Círculo: determina o custo da magia;
2. Rolagem afeta o bônus fornecido;
3. Área: se preenchido da forma correta ("cone Xm", "Y de 6m de raio"), gerará um template com a área da magia (afetado por aprimoramentos);
4. CD: determina o atributo a ser somado no cálculo e um bônus passado;

## Efeitos

São explicados com mais detalhes em [Efeitos]()

1. Efeitos de Uso: funcionam como aprimoramentos;
2. Efeitos Temporários: são os efeitos aplicados pela magia como (Armadura Arcana, Condição Pasmo, etc); Para o Foundry são efeitos que terão sua duração acompanhada;
3. Efeitos Passivos: são efeitos sempre presentes no personagem;
4. Efeitos Inativos: efeitos que são aplicados por aprimoramentos;

{{< figure src="../../images/ficha-aprimoramentos.jpg" width="512px" >}}


# Poderes
{{< figure src="../../images/ficha-poder.jpg" width="512px" >}}



<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>