---
title: "Armas"
weight: 2
---

## Detalhes

{{< figure src="../../images/ficha-armas.jpg" width="512px" class="float-right" >}}

1. Ataque: 
    - Altera o atríbuto somado na perícia;
    - Altera a perícia usada no ataque;
    - Outros bônus, aceita bônus numéricos, dados, ou [Características](../../avancado/data/);


## Encantos

Apenas os encanto Lancinante foi incluso, aplicá-lo fará com que os bônus numéricos do dano sejam multiplicados.

## Efeitos

Efeitos são explicados na seção [EFEITOS](../../efeitos/efeitos/);


<style>figure.float-right{float:right}nav.pagination{width:100%}.edit-meta{width:100%;float:right}</style>