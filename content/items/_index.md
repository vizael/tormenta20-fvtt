---
title: "Items"
weight: 3
---

Para o FoundryVTT Items são todo tipo de coisas que um Ator pode possuir;

Em Tormenta20 podem ser Armas, Armaduras, Classes, Consumíveis, Magias, Poderes e Tesouros.

Itens podem ser usados clicando em seus nomes na ficha, editados clicando no quadrado com um lápis, excluidos clicando na lixeira.

Itens e outros elementos que possuem rolagem, como atributos e perícias, podem ser configurados segurando SHIFT ao clicar no item.