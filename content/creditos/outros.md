---
title: "Outros"
weight: 3
---

- Atropos por desenvolver o Foundry VTT e o sistema de DnD5e;

- Arte de alguns ícones de magias: J. W. Bjerk (eleazzaar) -- www.jwbjerk.com/art -- find this and other open art at: http://opengameart.org

- sdenec pelo módulo Tidy5e Sheet, cujo código foi adaptado neste sistema.

- syl3r86 pelos módulos Compendium Browser e FavTab, que foram adaptados neste sistema.

- Tijmen Bok pelo sistema Pathfinder 1e for Foundry VTT, cujo código foi adaptado neste sistema.

- Artes de tokens do [Forgotten Adventures](https://www.forgotten-adventures.net/). This system uses token arts from [Forgotten Adventures](https://www.forgotten-adventures.net/).