---
title: "Jambo"
weight: 2
---

Tormenta20 é um sistema de RPG produzido pela [Jambo Editora](https://jamboeditora.com.br/)!

Nossos agradecimentos a Felipe Della Corte, Guilherme Dei Svaldi, Leonel Caldela, JM Trevisan, Marcelo Cassaro, Rafael Dei Svaldi e Rogério Saladino;