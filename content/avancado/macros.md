---
title: "Macros"
weight: 3
draft: true
---

Macros são automações, podem ser simples mensagens ou códigos em javascript incluidos na barra de atalhos;

Tormenta20 possui uma macro especifica para armas que permite editar configurações de um ataque;

```js
//UTILIZE OS CAMPOS ABAIXO PARA MODIFICAR UM ATAQUE
//VALORES SERÃO SOMADOS A CARACTEÍSTICA.
//INICIAR COM "=" SUBSTITUIRÁ O BÔNUS DA FICHA DA ARMA
game.tormenta20.rollItemMacro("Adaga",{
    'atq' : "0",
	'dadoDano' : "",
	'dano' : "0", 
	'margemCritico' : "0",
	'multCritico' : "0",
	'pericia' : ""
});
```

- Atq: somara na jogada de ataque;
- dano: somara na jogada de dano;
- dadoDano: Altera o dado de dano da arma, exemplo: 2d6;
- margemCritico: somara na jogada de ataque, exemplo: -2;
- multCritico: somara no multiplicador de crítico;
- pericia: substituirá o bônus de perícia, exemplo: "=pon"