---
title: "Variáveis"
weight: 2
---

No foundry você pode incluir atributos de personagens em rolagens.

Por exemplo `/r 1d20 + @for` somará o Modificador de Força do personagem selecionado na rolagem;

## Variáveis de Atores

As principais variáveis de Atores em Tormenta20 são:

| Váriavel                          | Cacácteristica                    |
| --------------------------------- | --------------------------------- |
| @nivel; @meionivel			          | Nível                             |
| @atributes.cd                     | CD (10 + meio nível + atributo)   |
| @for;@des;@con;@int;@sab;@car;    | Modificadores de Atributos        |
| @pericia.???.value                | Valor final de Perícia (ex: acr)  |
| @@atributoChave		                | Inclui o atributo de conjuração   |